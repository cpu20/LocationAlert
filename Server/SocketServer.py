import socket
import sys
import time
import json
import struct
from thread import *

HOST = ''    # Listen on all available interfaces
PORT = 7004  # Just a free port

# Thread used to handle incoming connections
def clientHandlerThread(conn):
    # Receive all data untill none is left
    conn.settimeout(0.1)
    recvData = []
    while True:
        try:
            data = conn.recv(4096)
            recvData.append(data)
        except socket.timeout:
            ''.join(recvData)   # Join all the data to parse it
            break
    conn.settimeout(None)
    # Split the response header
    recvData = recvData[0].split('\r\n')
    print(recvData)
    # Split the request head further to extract GET/POST and the request location
    request_head = recvData[0].split()
    body = recvData[len(recvData)-1]
    # The first element tells us if we got a GET or POST request
    if(request_head[0] == "GET"):
        # Open the file the user requested
        fileFound = True
        try:
            f = open(request_head[1][1:], 'r')
            msg = f.read()
        except IOError: # The file does not exist
            fileFound = False
            msg = ""

        # Clearly state that connection will be closed after this response,
        # and specify length of response body
        response_headers = {
            'Content-Type': 'application/json; charset=utf8',
            'Content-Length': len(msg),
            'Connection': 'close',
        }

        response_headers_raw = ''.join('%s: %s\n' % (k, v) for k, v in response_headers.iteritems())

        # Reply as HTTP/1.1 server, saying "HTTP OK" (code 200).
        response_proto = 'HTTP/1.1'
        if(fileFound == True):
            response_status = '200'
        else:
            response_status = '404'
        response_status_text = 'OK' # this can be random

        print("Sending response.\n")
        # sending all this stuff
        conn.send('%s %s %s' % (response_proto, response_status, response_status_text))
        conn.send(response_headers_raw)
        conn.send('\n') # to separate headers from body
        conn.send(msg)

        # and closing connection, as we stated before
        conn.close()
    elif(request_head[0] == "PUT"):
        try:
            f = open(request_head[1][1:], 'w') # Truncate the file if it already exists
            f.write(body)
            fileWritten = True
        except IOError:
            fileWritten = False
            
        msg = '{"status":"OK"}'

        # Clearly state that connection will be closed after this response,
        # and specify length of response body
        response_headers = {
            'Content-Type': 'application/json; charset=utf8',
            'Content-Length': len(msg),
            'Connection': 'close',
        }
        
        response_headers_raw = ''.join('%s: %s\n' % (k, v) for k, v in response_headers.iteritems())
        
        # Reply as HTTP/1.1 server, saying "HTTP OK" (code 200).
        response_proto = 'HTTP/1.1'
        if(fileWritten == True):
            response_status = '200'
        else:
            response_status = '404'
        response_status_text = 'OK' # this can be random

        print("Sending response.\n")
        # sending all this stuff
        conn.send('%s %s %s' % (response_proto, response_status, response_status_text))
        conn.send(response_headers_raw)
        conn.send('\n') # to separate headers from body
        conn.send(msg)
        
        conn.close()
        
        #f.write(recvData[0])

# Create the socket
soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Now bin the created socket to the specified host and port
try:
    soc.bind((HOST, PORT))
except socket.error, msg:
    print("Binding to the socket failed. Error code: " + str(msg[0]) + " Error message: " + msg[1])
    sys.exit(2)

# Now listen on the socket for incoming connections
soc.listen(5) # If the socket is busy keep a maximum of 5 connections waiting before refusing them

# Keep accepting incomping connections and start threads for them to be handled
while True:
    # Wait for an incoming connection (blocking function)
    try:
        conn, addr = soc.accept()
    except socket.error:
        break
    print("Connected with: " + addr[0] + ":" + str(addr[1]))

    # Start a new thread to handle data transfer with the client
    start_new_thread(clientHandlerThread, (conn,))
# If the while loop ever exits, close the connection
soc.close()
