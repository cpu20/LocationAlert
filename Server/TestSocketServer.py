import socket
import sys
import time
import json
from thread import *

HOST = ''    # Listen on all available interfaces
PORT = 7004  # Just a free port

# Thread used to handle incoming connections
def clientHandlerThread(conn):
    # Send a reply to the client so he knows he is connected
    conn.send("Successfully connected to the server. Welcome!\n")
    
    # Loop that waits for data, but times out if no reply was send in 30 seconds
    start = time.time()
    while((time.time() - start) <= 30):
        # Receive data from the client
        data = conn.recv(128)
        if not data:
            break
        # Check which command the client gave us
        if(data.rstrip() == "write"):
            fileName = conn.recv(128)   # Get the name for the file to which to store the data
            jfile = open(fileName.rstrip(), 'w') # Open or create the file and truncate the file if it already exists
            # Receive how long the file is. This is nescessary to know when to stop receiving.
            fileLength = conn.recv(128)
            receivedLength = 0
            while(int(fileLength) > receivedLength):
                data = conn.recv(1024) # Receive the data to be stored into the file
                jfile.write(data)
                receivedLength += len(data)
            jfile.close()
            break
        elif(data.rstrip() == "read"):
            fileName = conn.recv(128)   # Get the name of the file which to read from
            jfile = open(fileName.rstrip(), 'r')
            data = jfile.read()
            conn.sendall(data)
            conn.sendall(str(len(data)))
            jfile.close()
            break
        else:
            conn.sendall("Wrong command.\n")
    # Came out of the loop so close the connection
    conn.close()

# Create the socket
soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Now bin the created socket to the specified host and port
try:
    soc.bind((HOST, PORT))
except socket.error, msg:
    print("Binding to the socket failed. Error code: " + str(msg[0]) + " Error message: " + msg[1])
    sys.exit(2)

# Now listen on the socket for incoming connections
soc.listen(5) # If the socket is busy keep a maximum of 5 connections waiting before refusing them

# Keep accepting incomping connections and start threads for them to be handled
while True:
    # Wait for an incoming connection (blocking function)
    try:
        conn, addr = soc.accept()
    except socket.error:
        break
    print("Connected with: " + addr[0] + ":" + str(addr[1]))

    # Start a new thread to handle data transfer with the client
    start_new_thread(clientHandlerThread, (conn,))
# If the while loop ever exits, close the connection
soc.close()
