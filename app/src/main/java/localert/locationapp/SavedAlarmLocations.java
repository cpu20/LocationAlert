/* Copyright (C) 2017 Tijl Schepens, Bruce Helsen, Evert Matthys De Zutter */

/* This file is part of LocationAlert.
 *
 * LocationAlert is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LocationAlert is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package localert.locationapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import localert.locationapp.SavedAlarmLocationsContract.AlarmLocationsEntry;

public class SavedAlarmLocations extends SQLiteOpenHelper {
    /* The database version must be incremented every time the schema changes. */
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "localert.locationapp.SavedAlarmLocations.db";

    public SavedAlarmLocations(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(AlarmLocationsEntry.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /* When the database is upgraded just delete all entries for now. */
        db.execSQL(AlarmLocationsEntry.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /* Just delete the database again. */
        onUpgrade(db, oldVersion, newVersion);
    }
}
