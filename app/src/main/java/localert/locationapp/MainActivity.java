/* Copyright (C) 2017 Tijl Schepens, Bruce Helsen, Evert Matthys De Zutter */

/* This file is part of LocationAlert.
 *
 * LocationAlert is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LocationAlert is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package localert.locationapp;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import localert.locationapp.SavedAlarmLocationsContract.AlarmLocationsEntry;
import localert.locationapp.SavedAlarmLocationsContentProvider;

import java.io.UnsupportedEncodingException;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    /* Code to identify the location permission request. */
    private static final int RLP = 8973;
    /* Tag that is printed with log messages. */
    private static final String TAG = "MainActivity";
    /* ID used to identify the request for a location from the LocationSelect activity. */
    private static final int LOCATIONS_ID = 4820;
    /* Used to indicate if the alarm is enabled or disabled. */
    private static final int alarmEnabled = 1;
    private static final int alarmDisabled = 0;
    /* IP address fo the private back-end server. */
    private static final String LOCATIONS_URL = "http://192.168.1.13:7004";

    private Location mLastLocation;
    private TextView mCurrentAlarmLocation;
    private SharedPreferences prefs;
    private AdapterView.OnItemClickListener mLocationListClickedHandler;
    /* Values returned from the LocationSelect. */
    private String rlocationName = "NoInit";
    private double rlocationLatitude;
    private double rlocationLongitude;
    /* ListView related variables. */
    private ListView mLastLocationList;
    ArrayList<String> mLastLocationListItems;
    ArrayAdapter<String> mLastLocationAdapter;
    /* Service to monitor location. */
    private Intent checkLocationService;
    /* Broadcast receiver used to receive messages from the service. */
    private BroadcastReceiver mReceiver;
    private RequestQueue mRequestQueue;
    /* The userID is used to identify the user on the server. */
    private String UserID;

    /* Local status variables. */
    private int alarmEnabledDisabled = 0;   // Is the alarm enabled or disabled?
    private Switch toggleAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.AddAlarm);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* Launch the location selection activity when the button is pressed. */
                Intent LocationSelect = new Intent(MainActivity.this, LocationSelect.class);
                startActivityForResult(LocationSelect, LOCATIONS_ID);
            }
        });

        toggleAlarm = (Switch) findViewById(R.id.toggleAlarm);
        toggleAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    int locationPermCheck = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
                    if(locationPermCheck == PackageManager.PERMISSION_GRANTED) {
                        /* Start the service if the alarm is enabled and the user granted location access. */
                                /* Create the intent to start the checkLocation service. */
                        startCheckLocation();
                    }
                    else{
                        /* The user already denied access to the location, so we should try giving the user
                        and explanation of why we need it. */
                        if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)){
                            AlertDialog.Builder info;
                            info = new AlertDialog.Builder(MainActivity.this);
                            info.setTitle("Grant permission")
                                    .setMessage("The app needs location permission to be able to trigger " +
                                            "the set alarms.")
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, RLP);
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .show();
                        }
                        else {
                            /* Just request the permission without explanation. */
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, RLP);
                        }
                    }
                } else {
                    /* Stop the service if the user disables the alarm. */
                    if(checkLocationService != null)
                        stopService(checkLocationService);
                }
            }
        });

        mLastLocationList = (ListView) findViewById(R.id.LastLocationList);

        mLocationListClickedHandler = new AdapterView.OnItemClickListener() {
            /* Gets called when the user clicks on an item in the listview. */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /* The where clause. */
                String WHERE = AlarmLocationsEntry.LOCATION_COLUMN_NAME + " = ?";
                /* Arguments for the where clause. */
                String[] args = {
                        mLastLocationListItems.get(position)
                };
                Cursor cursor = getContentResolver().query(
                        SavedAlarmLocationsContentProvider.CONTENT_URI,
                        null,
                        WHERE,
                        args,
                        null
                );
                cursor.moveToNext();
                /* Retrieve the found location. */
                rlocationName = cursor.getString(cursor.getColumnIndex(AlarmLocationsEntry.LOCATION_COLUMN_NAME));
                rlocationLatitude = cursor.getDouble(cursor.getColumnIndex(AlarmLocationsEntry.LATITUDE_COLUMN_NAME));
                rlocationLongitude = cursor.getDouble(cursor.getColumnIndex(AlarmLocationsEntry.LONGITUDE_COLUMN_NAME));
                mCurrentAlarmLocation.setText(rlocationName);
                cursor.close();
            }
        };
        mLastLocationList.setOnItemClickListener(mLocationListClickedHandler);

        mCurrentAlarmLocation = (TextView)findViewById(R.id.CurrentAlarmLocation);

        /* Read out preferences that were stored. */
        prefs = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        alarmEnabledDisabled = prefs.getInt(getString(R.string.alarm_enabled_disabled), alarmDisabled);

        mLastLocationListItems = new ArrayList<String>();

        UserID = getIntent().getStringExtra("UserID");

        mRequestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                LOCATIONS_URL + "/" + UserID + ".json", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                /* First empty the database. */
                getContentResolver().delete(SavedAlarmLocationsContentProvider.CONTENT_URI, null, null);
                JSONArray mLocationResults;
                try {
                    mLocationResults = response.getJSONArray("Locations");
                    for(int i=0;i<mLocationResults.length();i++){
                        mLastLocationListItems.add(mLocationResults.getJSONObject(i).getString("Location"));
                        /* Store the received values in the database. */
                        ContentValues newLocation = new ContentValues();
                        newLocation.put(AlarmLocationsEntry.LOCATION_COLUMN_NAME, mLocationResults.getJSONObject(i).getString("Location"));
                        newLocation.put(AlarmLocationsEntry.LATITUDE_COLUMN_NAME, mLocationResults.getJSONObject(i).getString("Latitude"));
                        newLocation.put(AlarmLocationsEntry.LONGITUDE_COLUMN_NAME, mLocationResults.getJSONObject(i).getString("Longitude"));
                        getContentResolver().insert(SavedAlarmLocationsContentProvider.CONTENT_URI, newLocation);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                }
                Log.d(TAG, "Response: " + response.toString());
                SetLastLocationAdapter();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "JSON request failed: " + error.getMessage());
                LoadContentProviderData();
            }
        });
        mRequestQueue.add(jsonObjectRequest);
    }

    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                /* Extract the message from the sent intent. */
                int statusCheckLocation = intent.getIntExtra("message", 0);
                /* Check what status was received. */
                if(statusCheckLocation == -1){
                    Snackbar.make(findViewById(R.id.LastLocationList), "Can't set the alarm without location permissions.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    /* Uncheck the button as setting the alarm failed. */
                    toggleAlarm.setChecked(false);
                }
                else if(statusCheckLocation == 1){
                    Snackbar.make(findViewById(R.id.LastLocationList), "Alarm triggered.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        };
        /* Register the broadcast receiver. */
        this.registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        /* Unregister the broadcast receiver. */
        this.unregisterReceiver(mReceiver);
    }

    protected void onStop(){
        super.onStop();

        final JSONObject usersLocations = new JSONObject();

        /* Read out all the locations in the database to send them to the server. */
        /* Sort them on ID so that the last location comes in the top of the list. */
        String sortOrder = AlarmLocationsEntry._ID + " ASC";
        Cursor cursor = getContentResolver().query(
                SavedAlarmLocationsContentProvider.CONTENT_URI,
                null,
                null,
                null,
                sortOrder
        );
        JSONArray arrayEntry = new JSONArray();
        /* Go through al the items that were returned. */
        for(int i=0;i<cursor.getCount();i++){
            cursor.moveToNext();
            JSONObject entry = new JSONObject();
            try {
                entry.put("Location", cursor.getString(cursor.getColumnIndex(AlarmLocationsEntry.LOCATION_COLUMN_NAME)));
                entry.put("Latitude", cursor.getString(cursor.getColumnIndex(AlarmLocationsEntry.LATITUDE_COLUMN_NAME)));
                entry.put("Longitude", cursor.getString(cursor.getColumnIndex(AlarmLocationsEntry.LONGITUDE_COLUMN_NAME)));
                arrayEntry.put(entry);
                usersLocations.put("Locations", arrayEntry);
            } catch (JSONException e) {
                Log.d(TAG, "JSON exception: " + e.toString());
            }
        }
        cursor.close();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT,
                LOCATIONS_URL + "/" + UserID + ".json", usersLocations, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Got response from server: " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "JSON write request failed: " + error.getMessage());
            }
        });
        jsonObjectRequest.setTag(38792);
        mRequestQueue.add(jsonObjectRequest);

        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(getString(R.string.alarm_enabled_disabled), alarmEnabledDisabled);
        prefsEditor.apply();
    }

    protected void SetLastLocationAdapter(){
        /* Set up the adapter for the listview. */
        mLastLocationAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mLastLocationListItems);
        mLastLocationList.setAdapter(mLastLocationAdapter);
    }

    protected void LoadContentProviderData(){
        /* Read out all the locations in the database and put them in the listview. */
        /* We only need the location names. */
        String[] projection = {
                AlarmLocationsEntry.LOCATION_COLUMN_NAME
        };
        /* Sort them on ID so that the last location comes in the top of the list. */
        String sortOrder = AlarmLocationsEntry._ID + " DESC";
        Cursor cursor = getContentResolver().query(
                SavedAlarmLocationsContentProvider.CONTENT_URI,
                projection,
                null,
                null,
                sortOrder
        );
        /* Go through al the items that were returned. */
        for(int i=0;i<cursor.getCount();i++){
            cursor.moveToNext();
            mLastLocationListItems.add(cursor.getString(cursor.getColumnIndex(AlarmLocationsEntry.LOCATION_COLUMN_NAME)));
        }
        cursor.close();

        SetLastLocationAdapter();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case RLP:
                /* If the user cancels the request the grantResults array will just be empty. */
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /* Start the service if the alarm is enabled and the user granted location access. */
                    startCheckLocation();
                }
                else {
                    /* Everything that has to be disabled because we have no permission
                       goes here. */
                    toggleAlarm.setChecked(false);
                    Snackbar.make(findViewById(R.id.LastLocationList), "We are not spying on you...", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
        }
    }

    protected void startCheckLocation(){
        if(rlocationName.equals("NoInit")){
            Snackbar.make(findViewById(R.id.LastLocationList), "Please select a location.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            toggleAlarm.setChecked(false);
        } else{
            checkLocationService = new Intent(MainActivity.this, CheckLocation.class)
                    .putExtra("Longitude", rlocationLongitude)
                    .putExtra("Latitude", rlocationLatitude);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(checkLocationService);
            }
            else
                startService(checkLocationService);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case LOCATIONS_ID:
                if(resultCode == Activity.RESULT_OK){
                    /* Get the result from the search. */
                    rlocationName = data.getStringExtra("locationName");
                    rlocationLatitude = data.getDoubleExtra("locationLatitude", 0);
                    rlocationLongitude = data.getDoubleExtra("locationLongitude", 0);
                    mCurrentAlarmLocation.setText(rlocationName);
                    /* Search in the database for the location so see if it already exists. */
                    /* We only need the location name. */
                    String[] projection = {
                            AlarmLocationsEntry.LOCATION_COLUMN_NAME
                    };
                    /* The selection is the location name. */
                                    /* The where clause. */
                    String WHERE = AlarmLocationsEntry.LOCATION_COLUMN_NAME + " = ?";
                    /* Arguments for the where clause. */
                    String[] args = {
                            rlocationName
                    };
                    /* Sort them on ID so that the last location comes in the top of the list. */
                    String sortOrder = AlarmLocationsEntry._ID + " DESC";
                    Cursor cursor = getContentResolver().query(
                            SavedAlarmLocationsContentProvider.CONTENT_URI,
                            projection,
                            WHERE,
                            args,
                            sortOrder
                    );
                    if(cursor.getCount() == 0) {
                        /* Store the users search in the database. */
                        ContentValues newLocation = new ContentValues();
                        newLocation.put(AlarmLocationsEntry.LOCATION_COLUMN_NAME, rlocationName);
                        newLocation.put(AlarmLocationsEntry.LATITUDE_COLUMN_NAME, rlocationLatitude);
                        newLocation.put(AlarmLocationsEntry.LONGITUDE_COLUMN_NAME, rlocationLongitude);
                        getContentResolver().insert(SavedAlarmLocationsContentProvider.CONTENT_URI, newLocation);
                        /* Also add the new location to the listview of last used locations. */
                        mLastLocationListItems.add(0, rlocationName);
                        /* Refresh the listview. */
                        mLastLocationAdapter.notifyDataSetChanged();
                    }
                    cursor.close();
                } else if(resultCode == Activity.RESULT_CANCELED){
                    Snackbar.make(findViewById(R.id.LastLocationList), "No new location set.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                }
        }
    }
}
