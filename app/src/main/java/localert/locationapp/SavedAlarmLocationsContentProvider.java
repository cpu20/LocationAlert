/* Copyright (C) 2017 Tijl Schepens, Bruce Helsen, Evert Matthys De Zutter */

/* This file is part of LocationAlert.
 *
 * LocationAlert is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LocationAlert is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package localert.locationapp;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

import org.w3c.dom.Text;

import localert.locationapp.SavedAlarmLocationsContract.AlarmLocationsEntry;

public class SavedAlarmLocationsContentProvider extends ContentProvider {
    /* The name of the provider. */
    static final String PROVIDER_NAME = "localert.locationapp.SavedAlarmLocationsContentProvider";
    /* The full content provider URL. */
    static final String URL = "content://" + PROVIDER_NAME + "/SavedAlarmLocations";
    static final Uri CONTENT_URI = Uri.parse(URL);

    /* Codes that identify the different URI's. */
    static final int LOCATIONS = 1;
    static final int LOCATION_ID = 2;

    static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    /* All the URI patterns that our provider will recognize come here. # means one row is accessed
    from the table with # the row number.
     */
    static{
        uriMatcher.addURI(PROVIDER_NAME, "SavedAlarmLocations", LOCATIONS);
        uriMatcher.addURI(PROVIDER_NAME, "SavedAlarmLocations/#", LOCATION_ID);
    }

    /* Used to operate with the database. */
    private SavedAlarmLocations mSavedAlarmLocations;
    private SQLiteDatabase mSavedAlarmLocationsDB;

    public SavedAlarmLocationsContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        mSavedAlarmLocationsDB = mSavedAlarmLocations.getWritableDatabase();
        switch(uriMatcher.match(uri)){
            case LOCATIONS:
                /* Nothing has to be done here for now. */
                break;
            case LOCATION_ID:
                /* Make a WHERE clause for the SQL query. The ID of the row is the last part
                of the URI. So match this part with the _ID of the row. Also make sure to add
                AND to the statement if other WHERE conditions were already present.
                 */
                if(TextUtils.isEmpty(selection))
                    selection = AlarmLocationsEntry._ID + " = " + uri.getLastPathSegment();
                else
                    selection = selection + " AND " + AlarmLocationsEntry._ID + " = " + uri.getLastPathSegment();
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented uri:" + uri);

        }
        /* Delete the records an count how many have been deleted. */
        return mSavedAlarmLocationsDB.delete(AlarmLocationsEntry.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public String getType(Uri uri) {
        switch(uriMatcher.match(uri)) {
            case LOCATIONS:
                /* Get all locations in the database. */
                return "vnd.android.cursor.dir/localert.locationapp.SavedAlarmLocationsProvider.Location";
            case LOCATION_ID:
                /* Get a specific location. */
                return "vdn.android.cursor.item/localert.locationapp.SavedAlarmLocationsProvider.Location";
            default:
                throw new UnsupportedOperationException("Not yet implemented uri:" + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        mSavedAlarmLocationsDB = mSavedAlarmLocations.getWritableDatabase();
        switch(uriMatcher.match(uri)){
            /* The requested URI was for the whole table. */
            case LOCATIONS:
            case LOCATION_ID:
                /* Check for empty records. They should not be inserted into the database. */
                if(values.get(AlarmLocationsEntry.LOCATION_COLUMN_NAME) == null ||
                        values.get(AlarmLocationsEntry.LATITUDE_COLUMN_NAME) == null ||
                        values.get(AlarmLocationsEntry.LONGITUDE_COLUMN_NAME) == null)
                    throw new SQLException("Got an empty record to insert for uri:" + uri);
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented uri:" + uri);
        }
        /* Store the values that were given. */
        long rowID = mSavedAlarmLocationsDB.insert(AlarmLocationsEntry.TABLE_NAME, null, values);
        if(rowID == -1)
            throw new SQLException("Inserting data into the database failed for uri:" + uri);
        /* Construct the uri of where the record was inserted and return it. */
        return ContentUris.withAppendedId(CONTENT_URI, rowID);
    }

    @Override
    public boolean onCreate() {
        mSavedAlarmLocations = new SavedAlarmLocations(getContext());
        if(mSavedAlarmLocations == null)
            return false;
        else
            return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        mSavedAlarmLocationsDB = mSavedAlarmLocations.getReadableDatabase();
        switch(uriMatcher.match(uri)){
            /* The requested URI was for the whole table. */
            case LOCATIONS:
                /* If the request didn't specify a sort order we define one. */
                if(TextUtils.isEmpty(sortOrder))
                    sortOrder = AlarmLocationsEntry.LOCATION_COLUMN_NAME + " ASC";
                break;
            /* The requested URI was for a specific row in the table. */
            case LOCATION_ID:
                /* Make a WHERE clause for the SQL query. The ID of the row is the last part
                of the URI. So match this part with the _ID of the row. Also make sure to add
                AND to the statement if other WHERE conditions were already present.
                 */
                if(TextUtils.isEmpty(selection))
                    selection = AlarmLocationsEntry._ID + " = " + uri.getLastPathSegment();
                else
                    selection = selection + " AND " + AlarmLocationsEntry._ID + " = " + uri.getLastPathSegment();
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented uri:" + uri);
        }
        /* Now query the database with the constructed query. */
        Cursor cursor = mSavedAlarmLocationsDB.query(
                AlarmLocationsEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        mSavedAlarmLocationsDB = mSavedAlarmLocations.getWritableDatabase();
        switch(uriMatcher.match(uri)){
            case LOCATIONS:
                /* Check for empty records. They should not be inserted into the database. */
                if(values.get(AlarmLocationsEntry.LOCATION_COLUMN_NAME) == null ||
                        values.get(AlarmLocationsEntry.LATITUDE_COLUMN_NAME) == null ||
                        values.get(AlarmLocationsEntry.LONGITUDE_COLUMN_NAME) == null)
                    throw new SQLException("Got an empty record to insert for uri:" + uri);
                break;
            case LOCATION_ID:
                /* Check for empty records. They should not be inserted into the database. */
                if(values.get(AlarmLocationsEntry.LOCATION_COLUMN_NAME) == null ||
                        values.get(AlarmLocationsEntry.LATITUDE_COLUMN_NAME) == null ||
                        values.get(AlarmLocationsEntry.LONGITUDE_COLUMN_NAME) == null)
                    throw new SQLException("Got an empty record to insert for uri:" + uri);
                /* Make a WHERE clause for the SQL query. The ID of the row is the last part
                of the URI. So match this part with the _ID of the row. Also make sure to add
                AND to the statement if other WHERE conditions were already present.
                 */
                if(TextUtils.isEmpty(selection))
                    selection = AlarmLocationsEntry._ID + " = " + uri.getLastPathSegment();
                else
                    selection = selection + " AND " + AlarmLocationsEntry._ID + " = " + uri.getLastPathSegment();
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented uri:" + uri);

        }
        /* Delete the records an count how many have been deleted. */
        return mSavedAlarmLocationsDB.update(AlarmLocationsEntry.TABLE_NAME, values, selection, selectionArgs);
    }
}
