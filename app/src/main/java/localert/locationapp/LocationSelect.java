/* Copyright (C) 2017 Tijl Schepens, Bruce Helsen, Evert Matthys De Zutter */

/* This file is part of LocationAlert.
 *
 * LocationAlert is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LocationAlert is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package localert.locationapp;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;



public class LocationSelect extends AppCompatActivity {

    /* ToDo replace with custom URLs */
    private static final String BaseURL = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=";
    private static final String URLKey = "&key=AIzaSyCRN8GhKiL_lZrhJ_xOea40soqDBa5Ftds";
    /* Tag to identify the activity. */
    private static final String TAG = "LocationSelect";
    /* Tag to identify HTTP requests that are being queued. */
    private static final String HTTP_REQUEST_TAG = "Google Places webservice";

    private Button mPlaceSearchButton;
    private Button mMapLocationButton;

    private EditText mPlaceSearch;
    private ListView mFoundPlaces;
    /* Array to store the search results. */
    JSONArray mSearchResults;
    /* Queue used to store HTTP requests. */
    private RequestQueue mRequestQueue;
    private int PLACE_PICKER_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_select);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mPlaceSearch = (EditText)findViewById(R.id.PlaceSearch);
        mFoundPlaces = (ListView)findViewById(R.id.FoundPlaces);



        /* Array list to populate the listview. */
        final ArrayList<String> SearchResults = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, SearchResults);
        mFoundPlaces.setAdapter(adapter);

        mFoundPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /* Check that valid search results were returned first. */
                if(mSearchResults != null) {
                    try{
                        /* Extract all data to be sent. */
                        String location = (String) adapter.getItem(position);
                        double locationLatitude = mSearchResults.getJSONObject(position)
                                .getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                        double locationLongitude = mSearchResults.getJSONObject(position)
                                .getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                        Intent returnLocation = new Intent();
                        returnLocation.putExtra("locationName", location);
                        returnLocation.putExtra("locationLatitude", locationLatitude);
                        returnLocation.putExtra("locationLongitude", locationLongitude);
                        setResult(Activity.RESULT_OK, returnLocation);
                        finish();
                    } catch(JSONException e){
                        Log.d(TAG, e.getMessage());
                    }
                }
            }
        });

       mMapLocationButton = (Button)findViewById(R.id.PlaceFromMapButton);
       mMapLocationButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view){
                startPlacePickerActivity();

            }
    });


        mPlaceSearchButton = (Button)findViewById(R.id.PlaceSearchButton);
        mPlaceSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String SearchQuery = null;
                if(mPlaceSearch.getTextSize() > 0) {
                    try{
                        SearchQuery = URLEncoder.encode(mPlaceSearch.getText().toString(), "UTF-8");
                    } catch(UnsupportedEncodingException e){
                        Log.d(TAG, e.getMessage());
                    }
                    if(SearchQuery != null) {
                        mRequestQueue = Volley.newRequestQueue(LocationSelect.this);
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, BaseURL +
                                SearchQuery + URLKey, null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject jsonObject) {
                                        try{
                                            String status = jsonObject.getString("status");
                                            if(SearchResults.isEmpty() != true)
                                                SearchResults.clear();
                                            else{
                                                if(status.contentEquals("ZERO_RESULTS")) {
                                                    SearchResults.add("No results found.");
                                                    mSearchResults = null;
                                                }
                                                else if(!status.equals("OK")) {
                                                    SearchResults.add("An error occurred.");
                                                    mSearchResults = null;
                                                    Log.d(TAG, status);
                                                } else {
                                                    mSearchResults = jsonObject.getJSONArray("results");
                                                    for(int i=0;i<mSearchResults.length();i++){
                                                        SearchResults.add(mSearchResults.getJSONObject(i).getString("name"));
                                                    }
                                                }
                                            }
                                            /* Refresh the listview. */
                                            adapter.notifyDataSetChanged();
                                        } catch(JSONException e) {
                                            Log.d(TAG, e.getMessage());
                                        }
                                        //mTextView.setText("Response is:" + response.substring(0, 500));
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Snackbar.make(findViewById(R.id.PlaceSearchView),
                                                "HTTP request error occurred! Sorry :(", Snackbar.LENGTH_LONG).show();
                                        Log.d(TAG, "Error occurred while getting string request: " + error.getLocalizedMessage());
                                    }
                                }
                        );
                        jsonObjectRequest.setTag(HTTP_REQUEST_TAG);
                        mRequestQueue.add(jsonObjectRequest);
                    }
                    else{
                        Snackbar.make(findViewById(R.id.PlaceSearchView), "Invalid place search entered", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        mFoundPlaces.requestFocus();
                    }
                }
            }
        });
        mPlaceSearch.setHint("Enter place and location, e.g. Albert Heijn Gent");
        mPlaceSearch.requestFocus();
    }

    private void startPlacePickerActivity(){

        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
        try{
            Intent intent = intentBuilder.build(this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
            //mPlaceSearch.setText("werkt");

        }catch (Exception e){
            mPlaceSearch.setText("exception thrown");
            e.printStackTrace();
        }
    }

    @Override
    protected  void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            extractData(data);
        }
    }
    private void extractData(Intent data){
        double locationLatitude ;
        double locationLongitude;



        Place placeSelected = PlacePicker.getPlace(data, this);
        LatLng latLng = placeSelected.getLatLng();
        locationLatitude = latLng.latitude;
        locationLongitude = latLng.longitude;

        String location = placeSelected.getName().toString();

            try{
                /* Extract all data to be sent. */
                //  String location = (String) adapter.getItem(position);
                Intent returnLocation = new Intent();
                returnLocation.putExtra("locationName", location);
                returnLocation.putExtra("locationLatitude", locationLatitude);
                returnLocation.putExtra("locationLongitude", locationLongitude);
                setResult(Activity.RESULT_OK, returnLocation);
                finish();
            } catch(Exception e){
                Log.d(TAG, e.getMessage());
            }



    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mRequestQueue != null){
            mRequestQueue.cancelAll(HTTP_REQUEST_TAG);
        }
    }
}
