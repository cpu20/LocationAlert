/* Copyright (C) 2017 Tijl Schepens, Bruce Helsen, Evert Matthys De Zutter */

/* This file is part of LocationAlert.
 *
 * LocationAlert is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LocationAlert is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package localert.locationapp;

import android.*;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class CheckLocation extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    /* String to identify the service in the log. */
    private static final String TAG = "CheckLocation service";
    private GoogleApiClient mGoogleApiClient;
    /* This object receives interactions from the clients. */
    private final IBinder mBinder = new CheckLocationBinder();
    private LocationRequest mLocationRequest;
    private double rlocationLatitude;
    private double rlocationLongitude;
    private Location mCurrentLocation;
    private Ringtone mRingtone;
    private Vibrator mVibrator;
    /* ID for the notification. */
    private final int LOCATION_NOTIFICATION_ID = 5364;

    /* Class that the clients can access. */
    public class CheckLocationBinder extends Binder {
        /* Method to return the service object if requested. */
        CheckLocation getService() {
            return CheckLocation.this;
        }
    }

    public CheckLocation() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        /* Create an instance of the GoogleAPIClient and add location services to it. */
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            /* Connect to the google API client. */
            if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting())
                mGoogleApiClient.connect();
        }
        /* Check that we have location access. */
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Intent intentMsg = new Intent("android.intent.action.MAIN")
                    .putExtra("message", -1);
            this.sendBroadcast(intentMsg);
            /* Stop the service as it has no use... */
            this.stopSelf();
            return;
        }
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        Log.d(TAG, "Creating service finished.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        rlocationLongitude = intent.getDoubleExtra("Longitude", 0);
        rlocationLatitude = intent.getDoubleExtra("Latitude", 0);
        /* Make the service a foreground service. */
        Intent locationNotificationIntent = new Intent(this, CheckLocation.class);
        PendingIntent locationNotificationPendingIntent = PendingIntent.getActivity(
                this, 3467, locationNotificationIntent, 0);
        Notification locationNotification;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            /* For android Oreo we need to creat a notificaitno channel. */
            NotificationManager notManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            /* ID of the channel. */
            String id = "LocAlertChannel";
            /* This is visible to the user. */
            String name = getString(R.string.location_notification_title);
            /* Description of the channel. */
            String description = getString(R.string.location_notification_text);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel locAlertChannel = new NotificationChannel(id, name, importance);
            notManager.createNotificationChannel(locAlertChannel);
            locAlertChannel.setDescription(description);
            locationNotification = new Notification.Builder(this, id)
                    .setContentTitle(getText(R.string.location_notification_title))
                    .setContentText(getText(R.string.location_notification_text))
                    .setTicker("LocationAlarm")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentIntent(locationNotificationPendingIntent)
                    .build();
        }
        else {
            locationNotification = new Notification.Builder(this)
                    .setContentTitle(getText(R.string.location_notification_title))
                    .setContentText(getText(R.string.location_notification_text))
                    .setTicker("LocationAlarm")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentIntent(locationNotificationPendingIntent)
                    .build();
        }
        startForeground(LOCATION_NOTIFICATION_ID, locationNotification);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        if(mRingtone != null)
            mRingtone.stop();
        if(mVibrator != null)
            mVibrator.cancel();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Connected to google API services.");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(2000);
        /* The fastest update interval we will handle is 1s. */
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Intent intentMsg = new Intent("android.intent.action.MAIN")
                    .putExtra("message", -1);
            this.sendBroadcast(intentMsg);
            /* Stop the service as it has no use... */
            this.stopSelf();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(location != null) {
                    float[] distance = new float[3];
                    Location.distanceBetween(
                            location.getLatitude(),
                            location.getLongitude(),
                            rlocationLatitude,
                            rlocationLongitude,
                            distance);

                    Log.d(TAG, "Distance: " + distance);

                    if (distance[0] < 200) {
                        mGoogleApiClient.disconnect();
                        /* Create a ringtone to play. */
                        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                        if(alarmUri == null)
                            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            mRingtone = RingtoneManager.getRingtone(getApplicationContext(), alarmUri);
                            /* Create a vibrator and vibrate the phone. */
                        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        if (Build.VERSION.SDK_INT >= 26) {
                            long[] timings = {200, 400, 800, 400, 600};
                            int[] ampl = {150, 0, 150, 0, 150};
                            VibrationEffect vib = VibrationEffect.createWaveform(timings, ampl, 4);
                            mVibrator.vibrate(vib);
                        } else {
                            mVibrator.vibrate(10000);
                        }
                        /* Play the ringtone. */
                        mRingtone.play();
                        /* Send a broadcast to the main activity. */
                        Intent intentMsg = new Intent("android.intent.action.MAIN")
                                .putExtra("message", 1);
                        CheckLocation.this.sendBroadcast(intentMsg);
                    }
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mGoogleApiClient.disconnect();
        /* If we can't connect to the Google API client it is of no use to keep running. */
        stopSelf();
    }
}
