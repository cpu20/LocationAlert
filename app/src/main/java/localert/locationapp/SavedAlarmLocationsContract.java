/* Copyright (C) 2017 Tijl Schepens, Bruce Helsen, Evert Matthys De Zutter */

/* This file is part of LocationAlert.
 *
 * LocationAlert is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LocationAlert is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package localert.locationapp;

import android.provider.BaseColumns;

public final class SavedAlarmLocationsContract {
    /* The constructor is private so no one can accidentally instantiate the contract class. */
    private SavedAlarmLocationsContract() {}

    public static class AlarmLocationsEntry implements BaseColumns{
        /* Naming for the database. */
        public static final String TABLE_NAME = "SavedAlarmLocations";
        public static final String LOCATION_COLUMN_NAME = "Location";
        public static final String LATITUDE_COLUMN_NAME = "Latitude";
        public static final String LONGITUDE_COLUMN_NAME = "Longitude";
        /* Methods to create and maintain the database. */
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + AlarmLocationsEntry.TABLE_NAME + " (" +
                        AlarmLocationsEntry._ID + " INTEGER PRIMARY KEY," +
                        AlarmLocationsEntry.LOCATION_COLUMN_NAME + " TEXT," +
                        AlarmLocationsEntry.LATITUDE_COLUMN_NAME + " FLOAT," +
                        AlarmLocationsEntry.LONGITUDE_COLUMN_NAME + " FLOAT);";
        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + AlarmLocationsEntry.TABLE_NAME + ";";


    }

}
