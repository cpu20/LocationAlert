/* Copyright (C) 2017 Tijl Schepens, Bruce Helsen, Evert Matthys De Zutter */

/* This file is part of LocationAlert.
 *
 * LocationAlert is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * LocationAlert is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package localert.locationapp;

import android.location.Location;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LocationAPP_test {

    Location mLocation;
    public String LOCATION = "Hunkar Kebab";


    public static final double LAT = 37.377166;
    public static final double LNG = -122.086966;








    @Rule
    public ActivityTestRule<LoginActivity> loginActivityActivityTestRule =
            new ActivityTestRule<>(LoginActivity.class);


    @Test
    public void Loginworks() {

        onView(withId(R.id.email))
                .perform(typeText("foo@example.com"));
        onView(withId(R.id.password))
                .perform(typeText("hello1234"));
        onView(withId(R.id.email_sign_in_button)).perform(click());

        onView(withId(R.id.CurrentAlarmLocation)).check(matches(withText("Current alarm location")));


        onView(withId(R.id.CurrentAlarmLocation)).perform(ViewActions.pressBack());
        onView(withId(R.id.google_sign_in))
                .perform(click());
        onView(withId(R.id.CurrentAlarmLocation)).check(matches(withText("Current alarm location")));
        Log.i("UI_Test", "Signed in on the app");

    }


    @After
    public void AddlocationWorks() throws InterruptedException {
        onView(withId(R.id.AddAlarm)).perform(click());
        onView(withId(R.id.PlaceSearch)).perform(typeText(LOCATION));
        onView(withId(R.id.PlaceSearchButton)).perform((click()));
        Thread.sleep(3000);
        onData(anything()).inAdapterView(withId(R.id.FoundPlaces)).check(matches(withText(LOCATION))).perform(click());
        Log.i("UI_Test", "Location added");


    }




    @After
    public void SelectLocation()throws InterruptedException{

        onData(allOf(is(instanceOf(String.class)), is(LOCATION)))
                .inAdapterView(withId(R.id.LastLocationList))
                .perform(click());
        onView(withId(R.id.toggleAlarm)).perform(click());

        Log.i("UI_Test", "Location Selected and Alarm set");
        Log.i("UI_Test", "Now set the location of the emulator to latitude:" + LAT+ "longitude" + LNG );
        Thread.sleep(60000);




    }

}


